let fs = require('fs');
var process = require('process');
let intervals = require('./intervalsv3').intervals;
let queue = [];
let numberOfPortals = 15;
let averageServiceRateInMinutes = 1.67;
let scale = 0.0001; // Example: 0.001 scales 24 hours to about 2min, 0.0001 scales 24 hours to about 10 sec.
let serviceInterval = 60 / averageServiceRateInMinutes * 1000; // Time between queue.pop() in ms
serviceInterval = serviceInterval * scale;

let servicedPassengers = [];

let portals = [];

class Passenger {
    predictedWaitTime;
    leaveQueueTime;
    constructor(enterQueueTime, predictedWaitTime){
        this.enterQueueTime = enterQueueTime;
        this.predictedWaitTime = predictedWaitTime;
    }
}

class TimeHandler {
    simulatedTimeElapsed = 0;
    realTimeElapsed = 0;

    process(delta){
        this.realTimeElapsed += delta;
        this.simulatedTimeElapsed += delta / scale;
    }
}

let timeHandler = new TimeHandler();

class WaitPrediction {
    passengersArrivedLast15Min = 0;
    
    passengerCount = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; // Each element is count of passengers in one minute
    index = 0; // Index of passenger count minute

    interval // ms
    nextTimeOut = 0;
    t = 0; // time

    arrivalRate = 0;
    predictedWaitTime = 0;

    constructor(interval){
        this.interval = interval * scale;
    }

    process(delta){
        this.t += delta;
        if(this.t >= this.nextTimeOut){
            this.predict();
            this.nextTimeOut = this.t + this.interval;
        }
    }

    predict(){
        let passengersPer15 = this.passengerCount.reduce((x, y) => x + y);
        this.arrivalRate =  passengersPer15 / (15); // passengers / 15 min
        
        let p = this.arrivalRate / averageServiceRateInMinutes;
        let p0 = Math.pow(1 + p + ( Math.sqrt(p) / 2 * (1 - (p / numberOfPortals)) ), -1);
        let Lq = Math.pow(p, numberOfPortals + 1) / (this.fact(numberOfPortals - 1) * Math.sqrt(numberOfPortals - p)) * p0;
        this.predictedWaitTime = Lq / this.arrivalRate;

        if(this.predictedWaitTime == null || isNaN(this.predictedWaitTime)){
            this.predictedWaitTime = 10000;
        }

        // console.log("p", p, "p0", p0, "Lq", Lq);

        // console.log('queueLength:', queue.length, 'passengerCount:', passengersPer15, 'arrivalrate:', this.arrivalRate, 'predictedWaitTime:', this.predictedWaitTime);
        
        if(this.index === 14){
            this.passengerCount.shift();
            this.passengerCount.push(0);
        } else {
            this.index++;
        }
    }

    addPassenger(){
        this.passengerCount[this.index] += 1;
    }

    fact(num){
        if(num < 0) throw new Error("Invalid number provided to fact():", num);
        if(num === 0) return 1;

        return (num * this.fact(num - 1));
    }
}

let waitPrediction = new WaitPrediction(60000);

class QueueHandler {
    realStartTime;
    realStopTime;
    t; // time
    nextTimeOut = 0; // time for next event (put passenger in queue)
    index = 0;
    finished = false;

    constructor(){}

    start(t){
        this.t = t;
        this.realStartTime = new Date();
    }

    process(delta){
        this.t += delta;
        if(this.t >= this.nextTimeOut){
            this.finished = this.putPassengerInQueue();
            // console.log("t:", this.t, "delta:", delta, "nextTimeout:", this.nextTimeOut);
        }
    }

    putPassengerInQueue(){
        if(this.index >= intervals.length - 1){
            this.realStopTime = new Date();
            return true;
        }

        let interval = parseFloat(intervals[this.index]);
        // console.log("Add: ", this.index, "queue:", queue.length);
        this.index++;
        
        interval = interval * scale;
        this.nextTimeOut = this.t + interval;

        let passenger = new Passenger(timeHandler.simulatedTimeElapsed, waitPrediction.predictedWaitTime);
        queue.push(passenger);
        waitPrediction.addPassenger();
        
        return false;
    }
}

class Portal {
    passengers = [];
    t;
    nextTimeOut = 0;

    constructor(){}

    start(t){
        this.t = t;
    }

    process(delta){
        this.t += delta;
        if(this.t >= this.nextTimeOut){
            this.finishPassenger();
        }
        if(this.passengers.length < 5){
            this.getPassengerFromQueue();
        }
    }

    finishPassenger(){
        if(this.passengers.length > 0){
            let passenger = this.passengers.shift();
            let data = {enterQueueTime: passenger.enterQueueTime, leaveQueueTime: passenger.leaveQueueTime, waitTime: (passenger.leaveQueueTime - passenger.enterQueueTime) / scale, predictedWaitTime: passenger.predictedWaitTime, processTime: (timeHandler.simulatedTimeElapsed - passenger.leaveQueueTime)};
            servicedPassengers.push(data); // TODO: Lagre disse for senere analyse
        }
        let max = 15000; //ms
        let min = -15000; //ms
        let scaledDiff = (Math.random() * (max - min) + min) * scale;
        this.nextTimeOut = this.t + serviceInterval + scaledDiff;
    }

    getPassengerFromQueue(){
        if(queue.length > 0){
            let passenger = queue.shift();
            passenger.leaveQueueTime = timeHandler.simulatedTimeElapsed;
            this.passengers.push(passenger);
        }
    }
}

class StatusHandler{
    t = 0;
    nextTimeOut = 0;
    updateInterval;
    simulationData = [];

    constructor(updateInterval){
        this.updateInterval = updateInterval;
    }

    start(t){
        this.t = t;
    }

    process(delta){
        this.t += delta;
        if(this.t >= this.nextTimeOut){
            this.addStatus();
            this.nextTimeOut = this.t + this.updateInterval;
        }
    }

    addStatus(){
        let status = {timeSinceStart: this.t, queueLength: queue.length, portals: portals.map((portal) => portal.passengers.length), predictedWaitTime: waitPrediction.predictedWaitTime}
        this.simulationData.push(status);
        console.log(status);
    }

    writeToFile(){
        // Write to file
        let time = (new Date(Date.now() + 1*60*60*1000)).toISOString();
        time = time.replace(/:/g, "");
        let filename = 'simulationData-' + time +  '.json';
        let file = fs.createWriteStream("output/" + filename);
        file.write(JSON.stringify({
            portals: numberOfPortals,
            averageServiceRateInMinutes: averageServiceRateInMinutes,
            scale: scale, 
            simulationData: this.simulationData
        }));
        file.end();
    }
}

function setup() {
    setupPortals();
    main();
}

let queueHandler = new QueueHandler();

function start(t) {
    queueHandler.start(t);
    
    portals.forEach((portal) => {
        portal.start(t);
    });
}

function stepProcess(delta){
    queueHandler.process(delta);

    portals.forEach((portal) => {
        portal.process(delta);
    });
}

function setupPortals() {
    for(let i = 0; i < numberOfPortals; i++){
        portals.push(new Portal());
    }
}

function getTime(){
    let time = process.hrtime();
    return time[0] * 1000 + time[1] / 1000000;
}

function sleep(timeout){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, timeout);
    })
}

async function main(){
    let realStartTime = new Date();
    //console.log("sleep finished:", Date.now());
    let statusHandler = new StatusHandler(100);
    let run = true;
    let delta = 0;
    let t0 = getTime();
    let t1 = t0;
    start(t0);
    statusHandler.start(t0);
    while(run){
        t0 = getTime();

        // TODO: Technically this delta time becomes less correct for every computation done in the loop. Improve.
        waitPrediction.process(delta);
        timeHandler.process(delta);
        run = mainProcessFunction(delta);  
        statusHandler.process(delta);

        //console.log("sleep start:", Date.now());
        // await sleep(2);
        t1 = getTime();
        delta = t1 - t0;
    }
    
    let stopTime = new Date();

    console.log("Started at:", realStartTime, "stopped at:", stopTime, "time diff:", stopTime - realStartTime);

    console.log("Saving simulationData...");
    statusHandler.writeToFile();
    console.log("Done.");

}

let queueHandlerFinishedPrinted = false;

const jsArrayToCsv = (jsArray) => {
    const headers = Object.keys(jsArray[0]).reduce((x, y) => x + "," + y) + "\n";
    let rows = "";
    jsArray.forEach((object) => {
        rows += Object.values(object).reduce((x, y) => x + "," + y) + "\n";
    });

    return headers + rows;
}

function mainProcessFunction(delta) {
    if(queueHandler.finished){
        if(!queueHandlerFinishedPrinted) {
            console.log("Finished processing historic data, waiting for portals to handle passengers.");
            console.log("Processed passenger arrival data. Started:", queueHandler.realStartTime, "finished at:", queueHandler.realStopTime, "diff:", queueHandler.realStopTime - queueHandler.realStartTime);
            queueHandlerFinishedPrinted = true;
        }
        
        if(queue.length === 0){
            if(portals.every((portal) => portal.passengers.length === 0)){
                // finished
                console.log("Finished processing historic data. All passengers are flying.");
                console.log("Processed passenger arrival data. Started:", queueHandler.realStartTime, "finished at:", queueHandler.realStopTime, "diff:", queueHandler.realStopTime - queueHandler.realStartTime);
                // Write enterQueueTime and leaveQueueTime for each passenger to servicedPassengers.txt

                // Write to file
                let time = (new Date(Date.now() + 1*60*60*1000)).toISOString();
                time = time.replace(/:/g, "");
                let filename = 'servicedPassengers-' + time +  '.json';
                let file = fs.createWriteStream("output/" + filename);
                file.write(JSON.stringify({
                    portals: numberOfPortals,
                    averageServiceRateInMinutes: averageServiceRateInMinutes,
                    scale: scale, 
                    passengers: servicedPassengers
                }));
                file.end();

                filename = 'servicedPassengers-' + time +  '.csv';
                file = fs.createWriteStream("output/" + filename);
                file.write(jsArrayToCsv(servicedPassengers));
                file.end();



                return false;
            }
        }
    }

    stepProcess(delta);
    return true;
}

setup();