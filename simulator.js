let queue = [];
let numberOfPortals = 5;
let averageServiceRateInMinutes = 1.67;
let scale = 0.001; // Example: 0.001 scales 24 hours to about 2min, 0.0001 scales 24 hours to about 10 sec.
let serviceInterval = 60 / averageServiceRateInMinutes * 1000; // Time between queue.pop() in ms
serviceInterval = serviceInterval * scale;

let startTime;
let endTime;
let historyFinished = false;

let servicedPassengers = [];

class Portal {
    passengers = [];
    constructor(x1, y1, x2){
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
    }
}
let colors = ['#32a852', '#323ea8', '#a83273', '#a89c32', '#a84432']
class Passenger {
    leaveQueueTime;
    color;
    constructor(enterQueueTime){
        this.enterQueueTime = enterQueueTime;
        this.color = colors[Math.floor(Math.random() * (colors.length))]
    }
}
let portals = [];
function setup() {
    let width = Math.floor(windowWidth);
    let height = Math.floor(windowHeight-56);
    
    createCanvas(width, height);
    startTime = Date.now();
    handleQueue();

    setupPortals();
}

function setupPortals() {
    for(let i = 0; i < numberOfPortals; i++){
        portals.push(new Portal(30+i*60, 50, 70+i*60));
        portalServicePassenger(portals[i]);
    }
}

function portalServicePassenger(portal){
    let max = 15000; //ms
    let min = -15000; //ms
    let scaledDiff = (Math.random() * (max - min) + min) * scale;
    let timeout = serviceInterval + scaledDiff;
    setTimeout(() => {
        if(portal.passengers.length > 0){
            let passenger = portal.passengers.shift();
            let data = {enterQueueTime: passenger.enterQueueTime, leaveQueueTime: passenger.leaveQueueTime};
            servicedPassengers.push(data); // TODO: Lagre disse for senere analyse
        }
        portalServicePassenger(portal);
    }, timeout);
}


function draw() {
    //background('#0f0100');
    background('black');

    portals.forEach(portal => {
        stroke(400);
        line(portal.x1, 50, portal.x2, 50);

        if(portal.passengers.length < 5){
            if(queue.length > 0){
                let passenger = queue.shift();
                passenger.leaveQueueTime = Date.now();
                portal.passengers.push(passenger);
            }
        }
    });

    noStroke();
    portals.forEach(portal => {
        portal.passengers.forEach((passenger, i) => {
            fill(color(passenger.color));
            square(portal.x1 + 20, 50 + i*15, 10);
        });
    });

    queue.forEach((e,i) => {
        fill(color(e.color));
        square(300, 250+i*15, 10);
    });

    if(historyFinished && queue.length === 0){
        if(portals.every((portal) => portal.passengers.length === 0)){
            // finished
            console.log("Finished processing historic data. All passengers are flying.");
            localStorage.setItem('servicedPassengers', JSON.stringify({passengers: servicedPassengers}));   
        }
    }
}

let totalTime = 0;
let totalTimeUnscaled = 0;
let counter = 0;
function handleQueue(){
    // if(counter >= 1000){
    if(counter >= intervals.length){
        historyFinished = true;
        endTime = Date.now();
        console.log("History finished. Scale: ", scale, ",. Time used:", endTime-startTime, "ms");
        console.log("total scaled interval time:", totalTime);
        console.log("total interval time (not scaled):", totalTimeUnscaled);
        return;
    }
    let interval = parseFloat(intervals[counter]);
    totalTimeUnscaled += interval;
    interval = interval * scale;

    totalTime += interval;
    //console.log("Interval: ", interval);
    //if(counter % 1000 === 0){
    //    console.log("total time:", totalTime);
    //}

    counter++;

    setTimeout(function(){
        let passenger = new Passenger(Date.now());
        queue.push(passenger);
        handleQueue();
    }, interval);
}
