let servicedPassengers = JSON.parse(localStorage.getItem('servicedPassengers')).passengers;
let text = "enterQueueTime,leaveQueueTime\n";
servicedPassengers.forEach(passenger => {
    text += passenger.enterQueueTime + ',' + passenger.leaveQueueTime + '\n';
});

document.getElementById('data').innerText = text;
