
const jsArrayToCsv = (jsArray) => {
    const headers = Object.keys(jsArray[0]).reduce((x, y) => x + "," + y) + "\n";
    let rows = "";
    jsArray.forEach((object) => {
        rows += Object.values(object).reduce((x, y) => x + "," + y) + "\n";
    });

    return headers + rows;
}

const testArray = [{
    "enterQueueTime": 0,
    "leaveQueueTime": 0,
    "waitTime": 0,
    "predictedWaitTime": null,
    "processTime": 49031.00000694394
},
{
    "enterQueueTime": 49031.00000694394,
    "leaveQueueTime": 49031.00000694394,
    "waitTime": 0,
    "predictedWaitTime": null,
    "processTime": 27222.000248730183
}];

console.log(jsArrayToCsv(testArray));